# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

NON_PDF_PREVIEWABLE_MIME_TYPES = [
    ("application/vnd.ms-excel",),
    ("text/plain", ".asc"),
    ("text/plain", ".crt"),
    ("text/plain", ".pem"),
    ("text/plain", ".key"),
    ("text/plain", ".sql"),
    ("application/vnd.ms-excel.sheet.macroEnabled.12",),
    ("application/vnd.ms-excel.template.macroEnabled.12",),
    ("application/vnd.ms-excel.sheet.binary.macroEnabled.12",),
    ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",),
    ("application/vnd.openxmlformats-officedocument.spreadsheetml.template",),
    ("application/vnd.oasis.opendocument.spreadsheet",),
    ("text/xml",),
    ("text/css", ".css"),
    ("text/csv",),
    ("text/html",),
    ("application/vnd.ms-outlook",),
    ("application/octet-stream",),
    ("message/rfc822", ".eml"),
    ("application/msaccess",),
]
